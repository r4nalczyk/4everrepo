package com.atlassian.tutorial.helloworld;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.bc.issue.IssueService.IssueResult;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.sal.api.user.UserManager;

public class IssueFinderService extends JiraWebActionSupport {

	private static final Logger log = LoggerFactory
			.getLogger(IssueFinderService.class);
	private static final String NO_ISSUE_FOUND = "nie ma takego issue";

	private IssueService issueService;
	private com.atlassian.jira.user.util.UserManager jiraUserManager;
	private String summary;

	public IssueFinderService(IssueService issueService,
			com.atlassian.jira.user.util.UserManager jiraUserManager) {
		this.issueService = issueService;
		this.jiraUserManager = jiraUserManager;
	}

	@Override
	public User getLoggedInUser() {
		return null;
	}

	public String execute() throws Exception {
		String issueNumber = request.getParameter("number");
		IssueResult result = issueService.getIssue(
				jiraUserManager.getUser("admin"), issueNumber);
		if (result != null && result.getIssue() != null) {
			summary = result.getIssue().getSummary();
		} else {
			summary = NO_ISSUE_FOUND;
		}
		return "showIssue";
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

}
