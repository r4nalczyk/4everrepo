package com.atlassian.tutorial.helloworld;

import com.atlassian.crowd.embedded.api.User;

public class IssueInputService extends
		com.atlassian.jira.action.JiraActionSupport {

	@Override
	public User getLoggedInUser() {
		return null;
	}

	public String execute() throws Exception {
		return "inputIssueNumberForm";
	}

}
